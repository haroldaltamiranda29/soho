# language: es
@hu
Característica: como usuario de Soho quiero pode registrarme en la pagina para poder acceder a su tareas.
  @cp1
  Escenario: Registro
    Dado que el usuario quiere utilizar el CRM Zoho
    Cuando realizo el registro exitoso

    Entonces Verifico el acceso a la aplicación
  @cp2
  Escenario: Creación de Tarea
    Dado que ingreso al CRM Zoho con un usuario registrado
    Cuando realizo la creación de una Tarea
    Entonces verifico tarea creada exitosamente
package co.com.choucair.soho.stepdefinitions;

import co.com.choucair.soho.tasks.*;
import co.com.choucair.soho.questions.RegistroExito;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

public class SohoStepDefinitions {
    @Before
    public void IniciarEscenario(){

        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que el usuario quiere utilizar el CRM Zoho$")
    public void queElUsuarioQuiereUtilizarElCRMZoho(){
        OnStage.theActorCalled("grupo2").wasAbleTo(Abrir.paginaInicioSoho());
    }

    @Cuando("^realizo el registro exitoso$")
    public void realizoElRegistroExitoso(){
        OnStage.theActorInTheSpotlight().attemptsTo(Registro.exitoso());
    }

    @Entonces("^Verifico el acceso a la aplicación$")
    public void verificoElAccesoALaAplicación(){
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(RegistroExito.bienvenido()
                , Matchers.equalTo("Welcome Harold Altamiranda")));
    }

    @Dado("^que ingreso al CRM Zoho con un usuario registrado$")
    public void queIngresoAlCRMZohoConUnUsuarioRegistrado(){
        OnStage.theActorCalled("grupo2").wasAbleTo(IniciarSesion.paginaSoho(), Logueo.usuarioExistente());
    }

    @Cuando("^realizo la creación de una Tarea$")
    public void realizoLaCreaciónDeUnaTarea(){
        OnStage.theActorInTheSpotlight().attemptsTo(Crear.tarea());
    }

    @Entonces("^verifico tarea creada exitosamente$")
    public void verificoTareaCreadaExitosamente(){
    }

}

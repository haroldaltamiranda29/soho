package co.com.choucair.soho.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/features/registro_soho.feature",
        tags = "@hu",
        glue = "co.com.choucair.soho.stepdefinitions",
        snippets = SnippetType.CAMELCASE )

public class RunnerTags {
}
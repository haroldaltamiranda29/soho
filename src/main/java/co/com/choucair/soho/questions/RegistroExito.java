package co.com.choucair.soho.questions;

import co.com.choucair.soho.userinterface.PaginaSohoDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class RegistroExito implements Question<String> {

    public static RegistroExito bienvenido(){
        return new RegistroExito();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(PaginaSohoDatos.MENSAJE_BIENVENIDA).viewedBy(actor).asString();
    }
}

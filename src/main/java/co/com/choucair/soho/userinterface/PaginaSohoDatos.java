package co.com.choucair.soho.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaSohoDatos {
    
    public static final Target NOMBRE_COMPLETO = Target.the("Escribe el nombre")
            .located(By.id("namefield"));
    public static final Target CORREO = Target.the("Escribe el correo")
            .located(By.id("email"));
    public static final Target CONTRASEÑA = Target.the("Escribe la contraseña")
            .located(By.name("password"));
    public static final Target TELEFONO = Target.the("Selecciona pais")
            .located(By.id("mobile"));
    public static final Target POLITICA_PRIVACIDAD = Target.the("Click en la casilla de privacidad")
            .located(By.id("signup-termservice"));
    public static final Target EMPEZAR = Target.the("Click en el boton privacidad")
            .located(By.id("signupbtn"));
    public static final Target MENSAJE_BIENVENIDA = Target.the("Mensaje en la pagina")
            .located(By.xpath("//*[@id=\"show-uName\"]"));
    public static final Target INICIAR_SESION = Target.the("Click en iniciar sesion")
            .located(By.xpath("//*[@id=\"block-block-3\"]/div/div/div[3]/div[3]/a[1]"));
    public static final Target CORREO_ELECTRONICO = Target.the("Escribe el correo de inicio de sesion")
            .located(By.id("login_id"));
    public static final Target SIGUIENTE = Target.the("Click en el boton Siguiente")
            .located(By.xpath("//*[@id=\"nextbtn\"]/span"));
    public static final Target CONTRASENA = Target.the("Escribe la contraseña de inicio de sesion")
            .located(By.id("password"));
    public static final Target INICIAR_SESION_PAGINA_SOHO = Target.the("Click en el boton iniciar sesion")
            .located(By.id("nextbtn"));
    public static final Target CREAR = Target.the("Click boton crear")
            .located(By.id("createIcon"));
    public static final Target TAREA = Target.the("Click opcion tarea")
            .located(By.id("submenu_Tasks"));
    public static final Target CLICK_TEMA = Target.the("Click campo tema")
            .located(By.id("Crm_Tasks_SUBJECT"));
    public static final Target TIPO_TEMA = Target.the("Escoger el tema")
            .located(By.xpath("//*[@id=\"ui-id-1\"]/li[3]/div"));
    public static final Target FECHA_VENCIMIENTO = Target.the("Escoger fecha de vencimiento")
            .located(By.xpath("//*[@id=\"Crm_Tasks_DUEDATE\"]"));
    public static final Target CLICK_CONTACTO = Target.the("Dar click campo contacto")
            .located(By.xpath("//*[@id=\"Crm_Tasks_CONTACTID\"]"));
    public static final Target TIPO_CONTACTO = Target.the("Escoger tipo de contacto")
            .located(By.xpath("//*[@id=\"ui-id-2\"]/li/a/span"));
    public static final Target RECORDATORIO = Target.the("Click recordatorio")
            .located(By.xpath("//*[@id=\"Tasks_fldRow_REMINDAT\"]/div[2]/div/label/div"));
    public static final Target HECHO = Target.the("Click boton hecho")
            .located(By.id("rem_done"));
    public static final Target DESCRIPCION = Target.the("Descripcion")
            .located(By.xpath("//*[@id=\"Crm_Tasks_DESCRIPTION\"]"));
    public static final Target GUARDAR = Target.the("Boton guardar tarea")
            .located(By.id("saveTasksBtn"));
    public static final Target CLICK_CONTACT = Target.the("Descripcion")
            .located(By.xpath("//*[@id=\"secContent_Task_Information\"]"));
}

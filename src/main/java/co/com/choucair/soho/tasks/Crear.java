package co.com.choucair.soho.tasks;

import co.com.choucair.soho.userinterface.PaginaSohoDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Crear implements Task {
    public static Crear tarea() {
        return Tasks.instrumented(Crear.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PaginaSohoDatos.CREAR),
                Click.on(PaginaSohoDatos.TAREA),
                Click.on(PaginaSohoDatos.CLICK_TEMA),
                Click.on(PaginaSohoDatos.TIPO_TEMA),
                Enter.theValue("12/12/2021").into(PaginaSohoDatos.FECHA_VENCIMIENTO),
                Click.on(PaginaSohoDatos.CLICK_CONTACT),
                Click.on(PaginaSohoDatos.CLICK_CONTACTO),
                Click.on(PaginaSohoDatos.TIPO_CONTACTO),
                Click.on(PaginaSohoDatos.RECORDATORIO),
                Click.on(PaginaSohoDatos.HECHO),
                Enter.theValue("Se creo una cita con el contacto Kris Marrier").into(PaginaSohoDatos.DESCRIPCION),
                Click.on(PaginaSohoDatos.GUARDAR)
        );
        try {
            Thread.sleep (10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

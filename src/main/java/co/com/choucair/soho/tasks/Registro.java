package co.com.choucair.soho.tasks;

import co.com.choucair.soho.userinterface.PaginaSohoDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Registro implements Task {
    public static Registro exitoso() {
        return Tasks.instrumented(Registro.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue("Harold Altamiranda").into(PaginaSohoDatos.NOMBRE_COMPLETO),
                Enter.theValue("harold.altamiranda912@pascualbravo.edu.co").into(PaginaSohoDatos.CORREO),
                Enter.theValue("harold-123").into(PaginaSohoDatos.CONTRASEÑA),
                Enter.theValue("3004639870").into(PaginaSohoDatos.TELEFONO),
                Click.on(PaginaSohoDatos.POLITICA_PRIVACIDAD),
                Click.on(PaginaSohoDatos.EMPEZAR) );
    }
}

package co.com.choucair.soho.tasks;


import co.com.choucair.soho.userinterface.PaginaSohoDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Logueo implements Task {
    public static Logueo usuarioExistente() {
        return Tasks.instrumented(Logueo.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PaginaSohoDatos.INICIAR_SESION),
                Enter.theValue("harold.altamiranda912@pascualbravo.edu.co").into(PaginaSohoDatos.CORREO_ELECTRONICO),
                Click.on(PaginaSohoDatos.SIGUIENTE),
                Enter.theValue("harold-123").into(PaginaSohoDatos.CONTRASENA),
                Click.on(PaginaSohoDatos.INICIAR_SESION_PAGINA_SOHO)
        );
        try {
            Thread.sleep (60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

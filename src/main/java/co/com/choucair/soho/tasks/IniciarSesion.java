package co.com.choucair.soho.tasks;

import co.com.choucair.soho.userinterface.PaginaSoho;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class IniciarSesion implements Task {

    private PaginaSoho PaginaSoho;
    public static IniciarSesion paginaSoho() {
        return Tasks.instrumented(IniciarSesion.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(PaginaSoho));
    }
}
